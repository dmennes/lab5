#!/bin/expect

spawn mysql_secure_installation
expect "Enter current password for root (enter for none):"
send "\n"
expect "Set root password?"
send "y\r"
expect "New password:"
send "PASSWORD_TO_CHANGE\r"
expect "Re-enter new password:"
send "PASSWORD_TO_CHANGE\r"
expect "Remove anonymous users?"
send "y\r"
expect "Disallow root login remotely?"
send "y\r"
expect "Remove test database and access to it?"
send "y\r"
expect "Reload privilege tables now?"
send "y\r"
close
